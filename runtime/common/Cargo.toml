[package]
name = 'common-runtime'
description = 'Common code shared between all runtimes'
license = 'GPL-3.0-only'
version = '0.8.0-dev'
authors = ['Axiom-Team Developers <https://axiom-team.fr>']
edition = "2021"

[features]
runtime-benchmarks = [
	"frame-benchmarking/runtime-benchmarks",
    'frame-support/runtime-benchmarks',
    'frame-system-benchmarking/runtime-benchmarks',
    'frame-system/runtime-benchmarks',
	"pallet-babe/runtime-benchmarks",
    'pallet-balances/runtime-benchmarks',
    'pallet-certification/runtime-benchmarks',
    'pallet-duniter-wot/runtime-benchmarks',
    'pallet-identity/runtime-benchmarks',
    'pallet-membership/runtime-benchmarks',
    'pallet-multisig/runtime-benchmarks',
    'pallet-proxy/runtime-benchmarks',
    'pallet-treasury/runtime-benchmarks',
    'pallet-upgrade-origin/runtime-benchmarks',
    'sp-runtime/runtime-benchmarks',
]
std = [
    'codec/std',
    'duniter-primitives/std',
    'frame-support/std',
    'frame-system/std',
    'log/std',
    'pallet-authority-members/std',
    'pallet-babe/std',
    'pallet-balances/std',
    'pallet-certification/std',
    'pallet-duniter-account/std',
    'pallet-duniter-wot/std',
    'pallet-grandpa/std',
    'pallet-identity/std',
    'pallet-membership/std',
    'pallet-multisig/std',
    'pallet-oneshot-account/std',
    'pallet-provide-randomness/std',
    'pallet-proxy/std',
    'pallet-scheduler/std',
    'pallet-timestamp/std',
    'pallet-treasury/std',
	'pallet-universal-dividend/std',
	"serde/std",
	"serde_derive",
    'sp-arithmetic/std',
    'sp-core/std',
    'sp-membership/std',
    'sp-runtime/std',
    'sp-std/std',
]
try-runtime = [
	"frame-support/try-runtime",
	"frame-system/try-runtime",
	"pallet-babe/try-runtime",
	"pallet-grandpa/try-runtime",
]

[dependencies]
duniter-primitives = { path = '../../primitives/duniter', default-features = false }
pallet-authority-members = { path = '../../pallets/authority-members', default-features = false }
pallet-certification = { path = '../../pallets/certification', default-features = false }
pallet-duniter-account = { path = '../../pallets/duniter-account', default-features = false }
pallet-duniter-wot = { path = '../../pallets/duniter-wot', default-features = false }
pallet-identity = { path = '../../pallets/identity', default-features = false }
pallet-membership = { path = '../../pallets/membership', default-features = false }
pallet-oneshot-account = { path = '../../pallets/oneshot-account', default-features = false }
pallet-provide-randomness = { path = '../../pallets/provide-randomness', default-features = false }
pallet-upgrade-origin = { path = '../../pallets/upgrade-origin', default-features = false }
pallet-universal-dividend = { path = '../../pallets/universal-dividend', default-features = false }
sp-membership = { path = '../../primitives/membership', default-features = false }

# Crates.io
codec = { package = "parity-scale-codec", version = "3.1.5", features = ["derive"], default-features = false }
log = { version = "0.4.14", default-features = false }
scale-info = { version = "2.1.1", default-features = false, features = ["derive"] }
serde = { version = "1.0.101", default-features = false }
serde_derive = { version = "1.0.101", optional = true }
smallvec = "1.6.1"

# Substrate
frame-support = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
frame-system = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-babe = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-balances = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-grandpa = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-multisig = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-proxy = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-scheduler = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-session = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-timestamp = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
pallet-treasury = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
sp-arithmetic = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
sp-consensus-babe = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
sp-core = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
sp-runtime = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
sp-std = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }


# substrate benchmarks
frame-benchmarking = { git = "https://github.com/duniter/substrate", branch = 'duniter-substrate-v0.9.32', default-features = false, optional = true }
frame-system-benchmarking = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false, optional = true }

# TODO: there is a bad coupling in substrate that force to add this dependency
sp-staking = { git = 'https://github.com/duniter/substrate', branch = 'duniter-substrate-v0.9.32', default-features = false }
