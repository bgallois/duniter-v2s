# Xtask

We choose [`xtask`](https://github.com/matklad/cargo-xtask/) to run Rust scripts using `cargo`. To build these scripts, just run:

```bash
cargo xtask # this will build the scripts and show the available commands
```

These scripts mainly deal with runtime operations.